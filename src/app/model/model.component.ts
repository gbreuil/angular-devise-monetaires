import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";


export const Currencies = [ // Affichage d'une liste de devises
  {
    id: 1,
    name: 'Dollar',
    currency: '$',
    description: 'USA currency'
  },
  {
    id: 2,
    name: 'Euro',
    currency: '€',
    description: 'European currency'
  }
];

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})

export class ModelComponent implements OnInit {
  query!: { apikey: string; timestamp: number; base_currency: string; };
  data!: { currencyname: string; currencynumber: number; };
  post: any;
  currencies: any;
  response: any;
  rates: any;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.getCurrencies();
  }

  Currencies = Currencies;

  getCurrencies() { // Passer les noms et taux des devises dans des objets
    this.httpClient.get<any>('https://freecurrencyapi.net/api/v2/latest?apikey=2a84ea50-83fd-11ec-aa89-c5e7c6af51cd').subscribe(
      response => {
        this.post = response;
        this.response = this.post;
        this.post = Object.keys(this.post.data);
        this.currencies = this.post;
        //console.log(this.post); Afficher les devises dans la console
        this.response = Object["values"](this.response.data);
        this.rates = this.response;
        //console.log(this.rates); Afficher les taux dans la console
      }
    );
  }

  convertCurrency() {
    let test = this.rates
    console.log(test)

  }

  selectCurrency() {

  }
}
